package com.lucassonoda.AilinDelis.interfaces;

import java.util.List;

public interface ICrudService<T,E> {
	
	public List<T> findAll();
	public List<T> findByName(String name);
	public T findById(E id);
	public T save(T entity);
	public T update(T entity);
	public void delete(T entity);
}
