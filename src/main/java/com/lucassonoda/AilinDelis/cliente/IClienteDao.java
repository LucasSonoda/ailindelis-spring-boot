package com.lucassonoda.AilinDelis.cliente;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IClienteDao extends JpaRepository<ClienteEntity, Long> {

	public List<ClienteEntity> findByNombreContainingIgnoreCase(String nombre);
	
	
	@Query("select c, max(p.fechaEntrega) from ClienteEntity c left join fetch c.pedidos p "
			+ " group by c.id"
			+ " order by c.nombre")
	public List<Object[]> findAllDto();
	
	@Query("select c, max(p.fechaEntrega) from ClienteEntity c left join c.pedidos p "
			+ " group by c.id"
			+ " order by c.nombre")
	public Page<Object[]> findAllDto(Pageable pageable);
	
	@Query("select c, max(p.fechaEntrega) from ClienteEntity c left join fetch c.pedidos p "
			+ " where c.nombre like %:busqueda% or c.apellido like %:busqueda% or c.calle like %:busqueda% or c.localidad like %:busqueda%"
			+ " group by c.id"
			+ " order by c.nombre")
	public List<Object[]> findAllByBusqueda(@Param("busqueda") String busqueda);

	@Query("select c, max(p.fechaEntrega) from ClienteEntity c left join c.pedidos p "
			+ " where c.nombre like %:busqueda% or c.apellido like %:busqueda% or c.calle like %:busqueda% or c.localidad like %:busqueda%"
			+ " group by c.id"
			+ " order by c.nombre")
	public Page<Object[]> findAllByBusqueda(@Param("busqueda") String busqueda, Pageable pageable);
	
	@Query(value = "select c.id, c.nombre, c.apellido, c.calle, c.altura, c.localidad, c.origen, count(p.id), max(p.fecha_entrega)  "
			+ " from clientes c left join pedidos p on p.cliente_id = c.id"
			+ " group by c.id  order by count(p.id)  desc limit 0,10", nativeQuery = true)
	public List<Object[]> findTop10ConMasPedidos();
	
}
