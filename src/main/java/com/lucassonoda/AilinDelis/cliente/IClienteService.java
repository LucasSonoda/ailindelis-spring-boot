package com.lucassonoda.AilinDelis.cliente;

import java.util.List;

import com.lucassonoda.AilinDelis.cliente.dtos.ClienteGenericDto;
import com.lucassonoda.AilinDelis.cliente.dtos.PageableClienteDto;
import com.lucassonoda.AilinDelis.interfaces.ICrudService;

public interface IClienteService extends ICrudService<ClienteEntity, Long	> {
	
	public List<ClienteGenericDto> findAllDto();
	
	public PageableClienteDto findAll(int page, int itemsPerPage);
	
	public List<ClienteGenericDto> buscar(String busqueda);
	
	public PageableClienteDto buscar(String busqueda, int page, int size);
	
	public List<ClienteGenericDto> top10ConMasPedidos();
}
