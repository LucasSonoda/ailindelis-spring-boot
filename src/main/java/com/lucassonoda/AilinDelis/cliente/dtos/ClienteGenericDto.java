package com.lucassonoda.AilinDelis.cliente.dtos;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lucassonoda.AilinDelis.cliente.ClienteEntity;
import com.lucassonoda.AilinDelis.cliente.ClienteEntity.Origen;

public class ClienteGenericDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	private String nombre;
	private String apellido;

	private String calle;
	private String altura;
	private String localidad;

	private Origen origen;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date ultimoPedido;
	private int cantidadPedidos;

	public ClienteGenericDto() {

	}
	
	public ClienteGenericDto(ClienteEntity cliente){
		setId(cliente.getId());
		setNombre(cliente.getNombre());
		setApellido(cliente.getApellido());
		setCalle(cliente.getCalle());
		setAltura(cliente.getAltura());
		setLocalidad(cliente.getLocalidad());
		setOrigen(cliente.getOrigen());
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public Origen getOrigen() {
		return origen;
	}

	public void setOrigen(Origen origen2) {
		this.origen = origen2;
	}

	public Date getUltimoPedido() {
		return ultimoPedido;
	}

	public void setUltimoPedido(Date ultimoPedido) {
		this.ultimoPedido = ultimoPedido;
	}

	public int getCantidadPedidos() {
		return cantidadPedidos;
	}

	public void setCantidadPedidos(int cantidadPedidos) {
		this.cantidadPedidos = cantidadPedidos;
	}

}
