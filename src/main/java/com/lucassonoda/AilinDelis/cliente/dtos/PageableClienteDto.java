package com.lucassonoda.AilinDelis.cliente.dtos;

import java.io.Serializable;

import org.springframework.data.domain.Page;


public class PageableClienteDto implements Serializable {

	private Object clientes;
	private int paginasTotales;
	private int paginaActual;
	private boolean hasNext;
	private boolean hasPrevious;

	public PageableClienteDto() {

	}
	public PageableClienteDto(Object clientes, Page<Object[]> page) {
		this.clientes = clientes;
		this.hasNext = page.hasNext();
		this.hasPrevious = page.hasPrevious();
		this.paginasTotales = page.getTotalPages();
		this.paginaActual = page.getNumber();
	}

	public Object getClientes() {
		return clientes;
	}

	public void setClientes(Object clientes) {
		this.clientes = clientes;
	}

	public int getPaginasTotales() {
		return paginasTotales;
	}

	public void setPaginasTotales(int paginasTotales) {
		this.paginasTotales = paginasTotales;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	public boolean isHasNext() {
		return hasNext;
	}

	public void setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
	}

	public boolean isHasPrevious() {
		return hasPrevious;
	}

	public void setHasPrevious(boolean hasPrevious) {
		this.hasPrevious = hasPrevious;
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
