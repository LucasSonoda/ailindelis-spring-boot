
package com.lucassonoda.AilinDelis.cliente;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lucassonoda.AilinDelis.cliente.ClienteEntity.Origen;
import com.lucassonoda.AilinDelis.cliente.dtos.ClienteGenericDto;
import com.lucassonoda.AilinDelis.cliente.dtos.PageableClienteDto;

@Service
public class ClienteServiceImpl implements IClienteService {

	@Autowired
	private IClienteDao clienteDao;

	@Override
	public List<ClienteEntity> findAll() {
		return clienteDao.findAll();
	}

	@Override
	public List<ClienteGenericDto> findAllDto() {
		return clienteDao.findAllDto().stream().map(result -> {
			ClienteGenericDto clienteDto = new ClienteGenericDto((ClienteEntity) result[0]);
			clienteDto.setUltimoPedido((Date) result[1]);
			return clienteDto;
		}).collect(Collectors.toList());
	}

	@Override
	public PageableClienteDto findAll(int page, int itemsPerPage) {
		Pageable pageable = PageRequest.of(page, itemsPerPage);

		Page<Object[]> result = clienteDao.findAllDto(pageable);
		var clientes = result.getContent().stream().map(item -> {
			ClienteGenericDto clienteDto = new ClienteGenericDto((ClienteEntity) item[0]);
			clienteDto.setUltimoPedido((Date) item[1]);
			return clienteDto;
		}).collect(Collectors.toList());

		return new PageableClienteDto(clientes, result);
	}

	@Override
	public List<ClienteEntity> findByName(String name) {
		return clienteDao.findByNombreContainingIgnoreCase(name);
	}

	@Override
	public ClienteEntity findById(Long id) {
		return clienteDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public ClienteEntity save(ClienteEntity entity) {
		return clienteDao.save(entity);
	}

	@Override
	@Transactional
	public ClienteEntity update(ClienteEntity entity) {
		return clienteDao.save(entity);
	}

	@Override
	@Transactional
	public void delete(ClienteEntity entity) {
		clienteDao.delete(entity);
	}

	@Override
	public List<ClienteGenericDto> buscar(String busqueda) {

		return clienteDao.findAllByBusqueda(busqueda).stream().map(result -> {
			ClienteGenericDto clienteDto = new ClienteGenericDto((ClienteEntity) result[0]);
			clienteDto.setUltimoPedido((Date) result[1]);
			return clienteDto;
		}).collect(Collectors.toList());
	}

	@Override
	public PageableClienteDto buscar(String busqueda, int page, int size) {
		Pageable pageable = PageRequest.of(page, size);

		Page<Object[]> result = clienteDao.findAllByBusqueda(busqueda, pageable);
		
		var clientes = result.getContent().stream().map(items -> {
			ClienteGenericDto clienteDto = new ClienteGenericDto((ClienteEntity) items[0]);
			clienteDto.setUltimoPedido((Date) items[1]);
			return clienteDto;
		}).collect(Collectors.toList());
		
		return new PageableClienteDto(clientes, result);
	}

	@Override
	public List<ClienteGenericDto> top10ConMasPedidos() {

		var resultados = clienteDao.findTop10ConMasPedidos();

		var clientes = resultados.stream().map(r -> {

			ClienteGenericDto cliente = new ClienteGenericDto();
			cliente.setId(((BigInteger) r[0]).longValue());
			cliente.setNombre((String) r[1]);
			cliente.setApellido((String) r[2]);
			cliente.setCalle((String) r[3]);
			cliente.setAltura((String) r[4]);
			cliente.setLocalidad((String) r[5]);
			cliente.setOrigen(Origen.values()[(int) r[6]]);
			cliente.setCantidadPedidos(((BigInteger) r[7]).intValue());
			cliente.setUltimoPedido((Date) r[8]);

			return cliente;
		}).collect(Collectors.toList());

		return clientes;
	}

}
