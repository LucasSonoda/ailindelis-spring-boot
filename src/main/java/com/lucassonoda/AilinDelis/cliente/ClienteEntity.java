package com.lucassonoda.AilinDelis.cliente;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;
import com.lucassonoda.AilinDelis.pedido.PedidoEntity;
import com.lucassonoda.AilinDelis.utils.jsonview.View;

@Entity
@Table(name = "clientes")
public class ClienteEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonView({ View.PedidoListView.class, View.ClienteListView.class })
	private long id;
	@Column
	@JsonView({ View.PedidoListView.class, View.ClienteListView.class })
	private String nombre;
	@Column
	@JsonView({ View.PedidoListView.class, View.ClienteListView.class })
	private String apellido;
	@Column
	@JsonView({ View.ClienteListView.class })
	private Origen origen;
	@Column
	@JsonView({ View.PedidoListView.class, View.ClienteListView.class })
	private String calle;
	@Column
	@JsonView({ View.PedidoListView.class, View.ClienteListView.class })
	private String altura;
	@Column
	@JsonView({ View.PedidoListView.class, View.ClienteListView.class })
	private String localidad;
	@Column
	@JsonView({ View.PedidoSingleView.class, View.ClienteSingleView.class })
	private String entreCalle_1;
	@Column
	@JsonView({ View.PedidoSingleView.class, View.ClienteSingleView.class })
	private String entreCalle_2;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cliente")
	private List<PedidoEntity> pedidos;

	@Column
	@JsonView({ View.PedidoSingleView.class, View.ClienteSingleView.class })
	private String observacion;

	@Column
	private Date fechaCreacion;
	
	public enum Origen {
		Desconocido, Instagram, Facebook;
	}

	public ClienteEntity() {

	}
	
	@PrePersist
	void prePersist() {
		this.fechaCreacion = new Date();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Origen getOrigen() {
		return origen;
	}

	public Origen getOrigenByOrdinal(int ordinal) {

		if (ordinal < 0 || (Origen.values().length - 1) < ordinal)
			return Origen.Desconocido;

		return Origen.values()[ordinal];
	}

	public void setOrigen(Origen origen) {
		this.origen = origen;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getEntreCalle_1() {
		return entreCalle_1;
	}

	public void setEntreCalle_1(String entreCalle_1) {
		this.entreCalle_1 = entreCalle_1;
	}

	public String getEntreCalle_2() {
		return entreCalle_2;
	}

	public void setEntreCalle_2(String entreCalle_2) {
		this.entreCalle_2 = entreCalle_2;
	}

	public List<PedidoEntity> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<PedidoEntity> pedidos) {
		this.pedidos = pedidos;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
