package com.lucassonoda.AilinDelis.entrypoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lucassonoda.AilinDelis.producto.CategoriaEntity;
import com.lucassonoda.AilinDelis.producto.ICategoriaService;

@RestController
@RequestMapping("/categoria")
@CrossOrigin(origins = { "*" })
public class CategoriaRestController {
	
	@Autowired
	private ICategoriaService service;
	
	@GetMapping("/findById/{id}")
	public CategoriaEntity findById(@PathVariable long id) {
		return service.findById(id);
	}
	@GetMapping("/findByNombre/{nombre}")
	public List<CategoriaEntity> findByNombre(@PathVariable String nombre){
		return service.findByName(nombre);
	}
	
	@GetMapping("/findAll")
	public List<CategoriaEntity> findAll(){
		return service.findAll();
	}
	@PostMapping("/save")
	public CategoriaEntity save(@RequestBody CategoriaEntity categoria) {
		return service.save(categoria);
	}
	@PutMapping("/update")
	public CategoriaEntity update(@RequestBody CategoriaEntity categoria) {
		return service.update(categoria);
	}
	
}
