package com.lucassonoda.AilinDelis.entrypoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.lucassonoda.AilinDelis.pedido.IPedidoService;
import com.lucassonoda.AilinDelis.pedido.PedidoEntity;
import com.lucassonoda.AilinDelis.pedido.PedidoEntity.Estado;
import com.lucassonoda.AilinDelis.utils.jsonview.View;

@CrossOrigin(origins = { "*" })
@RequestMapping("/pedido")
@RestController
public class PedidoRestController {

	@Autowired
	private IPedidoService pedidoService;

	@GetMapping("/findAll")
	@JsonView(View.PedidoListView.class)
	public List<PedidoEntity> findAll() {
		return pedidoService.findAll();
	}

	@GetMapping("/findByName/{name}")
	public List<PedidoEntity> findByName(@PathVariable String name) {
		return pedidoService.findByName(name);
	}

	@GetMapping("/findByCliente/{idCliente}")
	@JsonView(View.PedidoByClienteView.class)
	public List<PedidoEntity> findByCliente(@PathVariable long idCliente) {
		return pedidoService.findByCliente(idCliente);
	}

	@GetMapping("/findById/{id}")
	@JsonView(View.PedidoSingleView.class)
	public PedidoEntity findById(@PathVariable long id) {
		return pedidoService.findById(id);
	}

	@PostMapping("/save")
	public PedidoEntity save(@RequestBody PedidoEntity entity) {
		return pedidoService.save(entity);
	}

	@PutMapping("/update")
	@JsonView(View.PedidoSingleView.class)
	public PedidoEntity update(@RequestBody PedidoEntity entity) {
		return pedidoService.update(entity);
	}

	@DeleteMapping("/delete")
	public void delete(@RequestBody PedidoEntity entity) {
		pedidoService.delete(entity);
	}

	@PutMapping("/cambiarEstado/{idPedido}/{estado}")
	public void cambiarEstado(@PathVariable long idPedido, @PathVariable String estado) throws Exception {
		try {
			System.out.println(estado);
			PedidoEntity pedido = pedidoService.findById(idPedido);
			pedido.setEstado(Estado.valueOf(estado));
			pedidoService.update(pedido);
		} catch (Exception e) {
			throw new Exception("Estado invalido");
		}
	}

	@GetMapping("/findAllByEstado/{estado}")
	@JsonView(View.PedidoListView.class)
	public List<PedidoEntity> findByEstado(@PathVariable Estado estado) {
		return pedidoService.findByEstado(estado);
	}

}
