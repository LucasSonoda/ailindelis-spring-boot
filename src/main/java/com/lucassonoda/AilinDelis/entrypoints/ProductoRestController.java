package com.lucassonoda.AilinDelis.entrypoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.lucassonoda.AilinDelis.producto.IProductoService;
import com.lucassonoda.AilinDelis.producto.ProductoEntity;
import com.lucassonoda.AilinDelis.producto.dto.ProductoGenericDto;
import com.lucassonoda.AilinDelis.utils.jsonview.View;

@CrossOrigin(origins = { "*" })
@RequestMapping("/producto")
@RestController
public class ProductoRestController {

	@Autowired
	private IProductoService productoService;

	@GetMapping("/findAll")
	@JsonView({View.ProductoListView.class})
	public List<ProductoEntity> findAll() {
		return productoService.findAll();
	}

	@GetMapping("/findByName/{name}")
	public List<ProductoEntity> findbyName(@PathVariable String name) {
		return productoService.findByName(name);
	}
	
	@GetMapping("/buscarProducto/{name}")
	public List<ProductoGenericDto> buscarProducto(@PathVariable String name){
		return productoService.findByNombreOrderByMasComprado(name);
	}
	
	@GetMapping("/findById/{id}")
	public ProductoEntity findById(@PathVariable long id) {
		return productoService.findById(id);
	}
	
	@GetMapping("/findTop10")
	public List<ProductoGenericDto> findTop10(){
		return productoService.findTop10();
	}

	@PostMapping("/save")
	public ProductoEntity save(@RequestBody ProductoEntity entity) {
		return productoService.save(entity);
	}

	@PutMapping("/update")
	public ProductoEntity update(@RequestBody ProductoEntity entity) {
		return productoService.update(entity);
	}
	@PutMapping("/bulk-update")
	public void bulkUpdate(@RequestBody List<ProductoEntity> productos) {
		productoService.bulkUpdate(productos);
	}

	@PostMapping("/delete")
	public void delete(@RequestBody ProductoEntity entity) {
		productoService.delete(entity);
	}
	
	@PostMapping("/inactivar")
	public void inactivar(@RequestBody ProductoEntity entity) {
		try {
			productoService.delete(entity);
		}catch (Exception e) {
			productoService.inactivarProducto(entity.getId());
		}
		
	}

}
