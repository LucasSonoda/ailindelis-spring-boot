package com.lucassonoda.AilinDelis.entrypoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.lucassonoda.AilinDelis.cliente.ClienteEntity;
import com.lucassonoda.AilinDelis.cliente.IClienteService;
import com.lucassonoda.AilinDelis.cliente.dtos.ClienteGenericDto;
import com.lucassonoda.AilinDelis.cliente.dtos.PageableClienteDto;
import com.lucassonoda.AilinDelis.utils.jsonview.View;

@CrossOrigin(origins = { "*" })
@RequestMapping("/cliente")
@RestController()
public class ClienteRestController {

	@Autowired
	private IClienteService clienteService;

	@GetMapping("/findAll")
	public List<ClienteGenericDto> findAll() {
		return clienteService.findAllDto();
	}

	@GetMapping("/findAll/{page}/{size}")
	public PageableClienteDto findAll(@PathVariable int page, @PathVariable int size) {
		return clienteService.findAll(page, size);
	}
	
	@GetMapping("/findByName/{name}")
	@JsonView({View.ClienteListView.class})
	public List<ClienteEntity> findByName(@PathVariable String name) {
		return clienteService.findByName(name);
	}

	@GetMapping("/findByBusqueda/{busqueda}")
	public List<ClienteGenericDto> findAllByBusqueda(@PathVariable String busqueda) {
		return clienteService.buscar(busqueda);
	}
	
	@GetMapping("/findByBusqueda/{busqueda}/{page}/{size}")
	public PageableClienteDto findAllByBusqueda(@PathVariable String busqueda, @PathVariable int page, @PathVariable int size) {
		return clienteService.buscar(busqueda,page, size);
	}
	
	
	@GetMapping("/findById/{id}")
	@JsonView({View.ClienteSingleView.class})
	public ClienteEntity findById(@PathVariable long id) {
		return clienteService.findById(id);
	}
	
	@GetMapping("/findTop10")
	public List<ClienteGenericDto> findTop10(){
		return clienteService.top10ConMasPedidos();
	}

	@PostMapping("/save")
	public ClienteEntity save(@RequestBody ClienteEntity entity) {
		return clienteService.save(entity);
	}

	@PutMapping("/update")
	public ClienteEntity update(@RequestBody ClienteEntity entity) {
		return clienteService.update(entity);
	}

	@PostMapping("/delete")
	public void delete(@RequestBody ClienteEntity entity) {
		clienteService.delete(entity);
	}

}
