package com.lucassonoda.AilinDelis.entrypoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lucassonoda.AilinDelis.estadisticas.IEstadisticasService;
import com.lucassonoda.AilinDelis.estadisticas.dto.FacturacionAcumuladaPorMes;
import com.lucassonoda.AilinDelis.estadisticas.dto.PedidosAcumuladosMesDto;
import com.lucassonoda.AilinDelis.estadisticas.dto.PedidosAcumuladosPorHoraDto;
import com.lucassonoda.AilinDelis.estadisticas.dto.infoPedidos;
import com.lucassonoda.AilinDelis.estadisticas.dto.PedidosAcumuladosPorDiaDto;

@RequestMapping("/estadisticas")
@RestController
@CrossOrigin(origins = { "*" })
public class EstadisticasRestController {
	
	@Autowired
	private IEstadisticasService service;
	
	@GetMapping("/pedidosAcumuladosPorMes")
	public List<PedidosAcumuladosMesDto> pedidosAcumuladosPorMes(){
		return service.PedidosAcumuladosPorMes();
	}
	@GetMapping("/pedidosAcumuladosPorDia")
	public List<PedidosAcumuladosPorDiaDto> pedidosAcumuladosPorDia(){
		return service.PedidosAcumuladosPorDia();
	}
	@GetMapping("/pedidosAcumuladosPorHora")
	public List<PedidosAcumuladosPorHoraDto> pedidosAcumuladosPorHora(){
		return service.PedidosAcumuladosPorHora();
	}
	@GetMapping("/facturacionAcumuladaPorMes")
	public List<FacturacionAcumuladaPorMes> facturacionAcumuladaPorMes(){
		return service.facturacionAcumuladaPorMes();
	}
	@GetMapping("/infoPedidos")
	public infoPedidos infoPedidos(){
		return service.infoPedidos();
	}
}
