package com.lucassonoda.AilinDelis.estadisticas.dto;

public class infoPedidos {

	private Double promedio;
	private long totalEntregados;
	private long totalCancelados;

	public infoPedidos() {
		super();
	}

	public infoPedidos(Double promedio, long totalEntregados, long totalCancelados) {
		super();
		this.promedio = promedio;
		this.totalEntregados = totalEntregados;
		this.totalCancelados = totalCancelados;
	}

	public Double getPromedio() {
		return promedio;
	}

	public void setPromedio(Double promedio) {
		this.promedio = promedio;
	}

	public long getTotalEntregados() {
		return totalEntregados;
	}

	public void setTotalEntregados(long totalEntregados) {
		this.totalEntregados = totalEntregados;
	}

	public long getTotalCancelados() {
		return totalCancelados;
	}

	public void setTotalCancelados(long totalCancelados) {
		this.totalCancelados = totalCancelados;
	}

}
