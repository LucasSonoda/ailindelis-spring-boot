package com.lucassonoda.AilinDelis.estadisticas.dto;

import com.lucassonoda.AilinDelis.estadisticas.utils.DateParse;

public class FacturacionAcumuladaPorMes {

	private String month;
	private Double acumulado;

	public FacturacionAcumuladaPorMes() {
		super();
	}

	public FacturacionAcumuladaPorMes(int month, Double acumulado) {
		super();
		this.month = DateParse.IntToMonth(month);
		this.acumulado = acumulado;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public void setMonth(int month) {
		this.month = DateParse.IntToMonth(month);
	}

	public Double getAcumulado() {
		return acumulado;
	}

	public void setAcumulado(Double acumulado) {
		this.acumulado = acumulado;
	}

}
