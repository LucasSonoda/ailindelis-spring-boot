package com.lucassonoda.AilinDelis.estadisticas.dto;

import java.io.Serializable;

import com.lucassonoda.AilinDelis.estadisticas.utils.DateParse;

public class PedidosAcumuladosPorDiaDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String day;
	private long cantPedidos;

	public PedidosAcumuladosPorDiaDto() {

	}

	public PedidosAcumuladosPorDiaDto(int day, long cantPedidos) {
		super();
		this.day = DateParse.IntToDayOfTheWeek(day);
		this.cantPedidos = cantPedidos;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}
	public void setDay(int day) {
		this.day = DateParse.IntToDayOfTheWeek(day);
	}

	public long getCantPedidos() {
		return cantPedidos;
	}

	public void setCantPedidos(long cantPedidos) {
		this.cantPedidos = cantPedidos;
	}
	


}
