package com.lucassonoda.AilinDelis.estadisticas.dto;

import java.io.Serializable;

import com.lucassonoda.AilinDelis.estadisticas.utils.DateParse;

public class PedidosAcumuladosMesDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int year;
	private String month;
	private long cantPedidos;

	public PedidosAcumuladosMesDto() {

	}

	public PedidosAcumuladosMesDto(int year, int month, long cantPedidos) {
		super();
		this.year = year;
		this.month = DateParse.IntToMonth(month);
		this.cantPedidos = cantPedidos;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}
	public void setMonth(int month) {
		this.month = DateParse.IntToMonth(month);
	}

	public long getCantPedidos() {
		return cantPedidos;
	}

	public void setCantPedidos(long cantPedidos) {
		this.cantPedidos = cantPedidos;
	}


}
