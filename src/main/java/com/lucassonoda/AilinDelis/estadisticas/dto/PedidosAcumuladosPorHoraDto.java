package com.lucassonoda.AilinDelis.estadisticas.dto;

import java.io.Serializable;

public class PedidosAcumuladosPorHoraDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int hour;

	private long cantPedidos;

	public PedidosAcumuladosPorHoraDto() {

	}

	public PedidosAcumuladosPorHoraDto(int hour, long cantPedidos) {
		super();
		this.hour = hour;
		this.cantPedidos = cantPedidos;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public long getCantPedidos() {
		return cantPedidos;
	}

	public void setCantPedidos(long cantPedidos) {
		this.cantPedidos = cantPedidos;
	}

}
