package com.lucassonoda.AilinDelis.estadisticas;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.lucassonoda.AilinDelis.pedido.PedidoEntity;

public interface IEstadisticasDao extends JpaRepository<PedidoEntity, Long> {

	@Query(value = ""
			+ " select YEAR(p.fecha_entrega), MONTH(p.fecha_entrega) as 'month' ,count(*) as 'cantPedidos' from pedidos p"
			+ " where p.estado = 1" + " group by MONTH(p.fecha_entrega)" + " order by p.fecha_entrega"
			+ " limit 0,6;", nativeQuery = true)
	public List<Object[]> pedidosAcumuladosPorMes();

	@Query(value = "" + " select DAYOFWEEK(p.fecha_entrega) as 'day' ,count(*) as 'cantPedidos' from pedidos p"
			+ " where p.estado = 1" + " group by DAYOFWEEK(p.fecha_entrega)" + " order by count(*) desc" + " limit 0,7;"
			+ "", nativeQuery = true)
	public List<Object[]> pedidosAcumuladosPorDia();

	@Query(value = "" + " select HOUR(p.fecha_entrega) as 'hour' ,count(*) as 'cantPedidos' from pedidos p"
			+ " where p.estado = 1" + " group by HOUR(p.fecha_entrega)" + " order by count(*) desc" + " limit 0,5;"
			+ "", nativeQuery = true)
	public List<Object[]> pedidosAcumuladosPorHora();

	@Query(value = "select month(p.fecha_entrega) ,sum((i.cantidad*i.precio)-((i.cantidad*i.precio)*(i.descuento/100)))"
			+ " from pedidos p left join pedido_items i on i.pedido_id = p.id" + " where p.estado = 1"
			+ " group by MONTH(p.fecha_entrega)" + " order by MONTH(p.fecha_entrega)" + " limit 0,12"
			+ "", nativeQuery = true)
	public List<Object[]> facturacionAcumuladaPorMes();

	@Query(value = "" + " select  (entregados.total / count(p.totalDias)) as 'Promedio pedidos', entregados.total as 'entregados' , cancelados.total as 'cancelados'"
			+ " from (select count(day(p.fecha_entrega)) as 'totalDias' from pedidos p where p.estado = 1 group by DAY(p.fecha_entrega)) p"
			+ " , (select count(*) as 'total' from pedidos p where p.estado = 1 )  entregados"
			+ " , (select count(p.id) as 'total' from pedidos p where p.estado = 2) cancelados" 
			+ "", nativeQuery = true)
	public List<Object[]> infoPedidos();
	

}
