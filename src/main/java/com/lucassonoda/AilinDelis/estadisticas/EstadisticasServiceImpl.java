package com.lucassonoda.AilinDelis.estadisticas;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lucassonoda.AilinDelis.estadisticas.dto.FacturacionAcumuladaPorMes;
import com.lucassonoda.AilinDelis.estadisticas.dto.PedidosAcumuladosMesDto;
import com.lucassonoda.AilinDelis.estadisticas.dto.PedidosAcumuladosPorHoraDto;
import com.lucassonoda.AilinDelis.estadisticas.dto.infoPedidos;
import com.lucassonoda.AilinDelis.estadisticas.dto.PedidosAcumuladosPorDiaDto;

@Service
public class EstadisticasServiceImpl implements IEstadisticasService {

	@Autowired
	private IEstadisticasDao dao;

	@Override
	public List<PedidosAcumuladosMesDto> PedidosAcumuladosPorMes() {
		List<PedidosAcumuladosMesDto> pedidos = dao.pedidosAcumuladosPorMes().stream()
				.map(r -> new PedidosAcumuladosMesDto((int) r[0], (int) r[1], ((BigInteger) r[2]).longValue()))
				.collect(Collectors.toList());
		return pedidos;
	}

	@Override
	public List<PedidosAcumuladosPorDiaDto> PedidosAcumuladosPorDia() {
		List<PedidosAcumuladosPorDiaDto> pedidos = dao.pedidosAcumuladosPorDia().stream()
				.map(r -> new PedidosAcumuladosPorDiaDto((int) r[0], ((BigInteger) r[1]).longValue()))
				.collect(Collectors.toList());
		return pedidos;
	}

	@Override
	public List<PedidosAcumuladosPorHoraDto> PedidosAcumuladosPorHora() {
		List<PedidosAcumuladosPorHoraDto> pedidos = dao.pedidosAcumuladosPorHora().stream()
				.map(r -> new PedidosAcumuladosPorHoraDto((int) r[0], ((BigInteger) r[1]).longValue()))
				.collect(Collectors.toList());
		return pedidos;
	}

	@Override
	public List<FacturacionAcumuladaPorMes> facturacionAcumuladaPorMes() {
		List<FacturacionAcumuladaPorMes> facturacion = dao.facturacionAcumuladaPorMes().stream()
				.map(r -> new FacturacionAcumuladaPorMes((int) r[0], (Double) r[1]))
				.collect(Collectors.toList());
		return facturacion;
	}

	@Override
	public infoPedidos infoPedidos() {
		var r = dao.infoPedidos().get(0);
		infoPedidos pedidos =  new infoPedidos(((BigDecimal) r[0]).doubleValue(), ((BigInteger) r[1]).longValue(), ((BigInteger) r[2]).longValue());
		return pedidos;
	}

}
