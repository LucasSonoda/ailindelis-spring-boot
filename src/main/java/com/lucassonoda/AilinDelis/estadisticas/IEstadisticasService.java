package com.lucassonoda.AilinDelis.estadisticas;

import java.util.List;

import com.lucassonoda.AilinDelis.estadisticas.dto.FacturacionAcumuladaPorMes;
import com.lucassonoda.AilinDelis.estadisticas.dto.PedidosAcumuladosMesDto;
import com.lucassonoda.AilinDelis.estadisticas.dto.PedidosAcumuladosPorHoraDto;
import com.lucassonoda.AilinDelis.estadisticas.dto.infoPedidos;
import com.lucassonoda.AilinDelis.estadisticas.dto.PedidosAcumuladosPorDiaDto;

public interface IEstadisticasService {
	
	public List<PedidosAcumuladosMesDto> PedidosAcumuladosPorMes();
	
	public List<PedidosAcumuladosPorDiaDto> PedidosAcumuladosPorDia();
	
	public List<PedidosAcumuladosPorHoraDto> PedidosAcumuladosPorHora();

	public List<FacturacionAcumuladaPorMes> facturacionAcumuladaPorMes();
	
	public infoPedidos infoPedidos();
}
