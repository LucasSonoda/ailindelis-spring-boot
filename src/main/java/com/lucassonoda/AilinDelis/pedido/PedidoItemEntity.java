package com.lucassonoda.AilinDelis.pedido;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;
import com.lucassonoda.AilinDelis.producto.ProductoEntity;
import com.lucassonoda.AilinDelis.utils.jsonview.View;

@Entity
@Table(name="pedido_items")
public class PedidoItemEntity implements Serializable {

	@Id
	@JsonView({View.PedidoListView.class, View.PedidoByClienteView.class})
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "producto_id")
	@JsonView(View.PedidoListView.class)
	private ProductoEntity producto;
	@Column
	@JsonView({View.PedidoListView.class, View.PedidoByClienteView.class})
	private int cantidad;
	@Column 
	@JsonView({View.PedidoListView.class, View.PedidoByClienteView.class})
	private Double precio;
	@Column
	@JsonView({View.PedidoListView.class, View.PedidoByClienteView.class})
	private int descuento;
	
	public PedidoItemEntity() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ProductoEntity getProducto() {
		return producto;
	}

	public void setProducto(ProductoEntity producto) {
		this.producto = producto;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
}
