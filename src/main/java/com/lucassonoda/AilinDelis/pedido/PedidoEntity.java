package com.lucassonoda.AilinDelis.pedido;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.lucassonoda.AilinDelis.cliente.ClienteEntity;
import com.lucassonoda.AilinDelis.utils.jsonview.View;

@Entity
@Table(name = "pedidos")
public class PedidoEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonView({View.PedidoListView.class, View.PedidoByClienteView.class})
	private long id;
	@Column
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	@JsonView({View.PedidoSingleView.class, View.PedidoByClienteView.class})
	private Date fechaCreacion;
	@Column
	@JsonView(View.PedidoSingleView.class)
	private boolean pago;
	@Column
	@JsonView({View.PedidoListView.class, View.PedidoByClienteView.class})
	private Double sena;
	@Column
	@JsonView({View.PedidoSingleView.class, View.PedidoByClienteView.class})
	private TipoPago tipoPago;
	@Column
	@JsonView(View.PedidoSingleView.class)
	private TipoEntrega tipoEntrega;
	@Column
	@JsonView({View.PedidoListView.class, View.PedidoByClienteView.class})
	private Double costoEntrega;
	@Column
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	@JsonView({View.PedidoListView.class, View.PedidoByClienteView.class})
	private Date fechaEntrega;
	@Column
	@JsonView(View.PedidoSingleView.class)
	private String Observacion;
	@Column
	@JsonView({View.PedidoListView.class, View.PedidoByClienteView.class})
	private Estado estado;
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonView(View.PedidoListView.class)
	private ClienteEntity cliente;
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL )
	@JoinColumn(name = "pedido_id")
	@JsonView({View.PedidoListView.class, View.PedidoByClienteView.class})
	private List<PedidoItemEntity> items;

	@PrePersist
	void prePersist() {
		fechaCreacion = new Date();
	}
	
	public enum Estado {
		Pendiente, Entregado, Cancelado
	}
	public enum TipoEntrega{
		Retira, EnvioADomicilio
	}
	public enum TipoPago{
		Efectivo, MercadoPago
	}

	public PedidoEntity() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public boolean isPago() {
		return pago;
	}

	public void setPago(boolean pago) {
		this.pago = pago;
	}

	public Double getSena() {
		return sena;
	}

	public void setSena(Double sena) {
		this.sena = sena;
	}

	public TipoPago getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(TipoPago tipoPago) {
		this.tipoPago = tipoPago;
	}

	public TipoEntrega getTipoEntrega() {
		return tipoEntrega;
	}

	public void setTipoEntrega(TipoEntrega tipoEntrega) {
		this.tipoEntrega = tipoEntrega;
	}

	public Double getCostoEntrega() {
		return costoEntrega;
	}

	public void setCostoEntrega(Double costoEntrega) {
		this.costoEntrega = costoEntrega;
	}

	public Date getFechaEntrega() {
		return fechaEntrega;
	}

	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}


	public String getObservacion() {
		return Observacion;
	}

	public void setObservacion(String observacion) {
		Observacion = observacion;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public ClienteEntity getCliente() {
		return cliente;
	}

	public void setCliente(ClienteEntity cliente) {
		this.cliente = cliente;
	}

	public List<PedidoItemEntity> getItems() {
		return items;
	}

	public void setItems(List<PedidoItemEntity> items) {
		this.items = items;
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
