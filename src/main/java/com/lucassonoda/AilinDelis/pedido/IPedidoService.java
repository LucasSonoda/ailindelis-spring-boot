package com.lucassonoda.AilinDelis.pedido;

import java.util.List;

import com.lucassonoda.AilinDelis.interfaces.ICrudService;
import com.lucassonoda.AilinDelis.pedido.PedidoEntity.Estado;

public interface IPedidoService extends ICrudService<PedidoEntity, Long> {
	
	public List<PedidoEntity> findByCliente(long idCliente);
	
	public List<PedidoEntity> findByEstado(Estado estado);
	
	public void actualizarEstado(long idPedido, Estado estado);
}
