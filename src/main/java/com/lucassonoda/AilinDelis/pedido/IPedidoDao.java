package com.lucassonoda.AilinDelis.pedido;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.lucassonoda.AilinDelis.pedido.PedidoEntity.Estado;

public interface IPedidoDao extends JpaRepository<PedidoEntity, Long>{
	
	
	@Query("select distinct p from PedidoEntity p inner join fetch p.items i join fetch i.producto inner join fetch p.cliente")
	public List<PedidoEntity> findAllFetch();
	
	@Query("select distinct p from PedidoEntity p join fetch p.items i join fetch p.cliente c where c.id = ?1 order by p.fechaEntrega desc")
	public List<PedidoEntity> findByIdCliente(long idCliente);
	
	@Query("select distinct p from PedidoEntity p inner join fetch p.items i join fetch i.producto inner join fetch p.cliente where p.estado = ?1 order by p.fechaEntrega desc")
	public List<PedidoEntity> findByEstado(Estado estado);

	@Query("select p from PedidoEntity p join fetch p.cliente join fetch p.items i join fetch i.producto pto where p.id = ?1")
	public Optional<PedidoEntity> findByIdFetch(long id);
	
}
