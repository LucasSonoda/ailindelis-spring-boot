package com.lucassonoda.AilinDelis.pedido;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lucassonoda.AilinDelis.cliente.ClienteEntity;
import com.lucassonoda.AilinDelis.cliente.IClienteService;
import com.lucassonoda.AilinDelis.pedido.PedidoEntity.Estado;

@Service
public class PedidoServiceImpl implements IPedidoService {

	@Autowired
	private IPedidoDao pedidoDao;
	@Autowired
	private IClienteService clienteService;

	@Override
	public List<PedidoEntity> findAll() {
		return pedidoDao.findAllFetch();
	}

	@Override
	public List<PedidoEntity> findByName(String name) {
		return null;
	}

	@Override
	public PedidoEntity findById(Long id) {
		return pedidoDao.findByIdFetch(id).orElse(null);
	}

	@Override
	@Transactional
	public PedidoEntity save(PedidoEntity entity) {
		ClienteEntity cliente = entity.getCliente();
		
		if(cliente.getId()==0)
			clienteService.save(cliente);
		
		return pedidoDao.save(entity);
	}

	@Override
	@Transactional
	public PedidoEntity update(PedidoEntity entity) {
		
		
		return pedidoDao.save(entity);
	}

	@Override
	@Transactional
	public void delete(PedidoEntity entity) {
		pedidoDao.delete(entity);
	}

	@Override
	public List<PedidoEntity> findByCliente(long idCliente) {
		return pedidoDao.findByIdCliente(idCliente);
	}

	@Override
	public List<PedidoEntity> findByEstado(Estado estado) {
		return pedidoDao.findByEstado(estado);
	}

	@Override
	public void actualizarEstado(long idPedido, Estado estado) {
		PedidoEntity pedido = this.pedidoDao.findByIdFetch(idPedido).orElse(null);

		if (pedido == null)
			return;

		pedido.setEstado(estado);
		this.pedidoDao.save(pedido);
	}

}
