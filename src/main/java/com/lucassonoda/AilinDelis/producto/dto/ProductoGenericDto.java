package com.lucassonoda.AilinDelis.producto.dto;

import java.io.Serializable;

import com.lucassonoda.AilinDelis.producto.ProductoEntity;
import com.lucassonoda.AilinDelis.producto.ProductoEntity.Tamano;

public class ProductoGenericDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private long id;
	private String nombre;
	private Tamano tamano;
	private Double precioVenta;
	private long cantidadVendida;

	public ProductoGenericDto() {}
	
	public ProductoGenericDto(ProductoEntity producto) {
		setId(producto.getId());
		setNombre(producto.getNombre());
		setTamano(producto.getTamano());
		setPrecioVenta(producto.getPrecioVenta());
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Tamano getTamano() {
		return tamano;
	}

	public void setTamano(Tamano tamano) {
		this.tamano = tamano;
	}

	public Double getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public long getCantidadVendida() {
		return cantidadVendida;
	}

	public void setCantidadVendida(long cantidadVendida) {
		this.cantidadVendida = cantidadVendida;
	}

}
