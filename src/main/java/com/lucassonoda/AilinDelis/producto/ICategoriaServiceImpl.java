package com.lucassonoda.AilinDelis.producto;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class ICategoriaServiceImpl implements ICategoriaService {

	@Autowired
	private ICategoriaDao categoriaDao;
	
	@Override
	public List<CategoriaEntity> findAll() {
		return categoriaDao.findAll();
	}

	@Override
	public List<CategoriaEntity> findByName(String name) {
		return categoriaDao.findByNombre(name);
	}

	@Override
	public CategoriaEntity findById(Long id) {
		return categoriaDao.findById(id).orElse(null);
	}

	@Override
	public CategoriaEntity save(CategoriaEntity entity) {
		return categoriaDao.save(entity);
	}

	@Override
	public CategoriaEntity update(CategoriaEntity entity) {
		return categoriaDao.save(entity);
	}

	@Override
	public void delete(CategoriaEntity entity) {
		categoriaDao.delete(entity);
	}

}
