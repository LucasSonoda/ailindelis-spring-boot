package com.lucassonoda.AilinDelis.producto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lucassonoda.AilinDelis.producto.ProductoEntity.Tamano;
import com.lucassonoda.AilinDelis.producto.dto.ProductoGenericDto;

@Service
public class ProductoServiceImpl implements IProductoService {

	@Autowired
	private IProductoDao productoDao;
	
	@PersistenceContext 
	private EntityManager entityManager;
	
	private final int BATCH_SIZE = 5;

	@Override
	public List<ProductoEntity> findAll() {
		return productoDao.findAllFetch();
	}

	@Override
	public List<ProductoEntity> findByName(String name) {
		return productoDao.findByNombreContainingIgnoreCase(name);
	}

	@Override
	public ProductoEntity findById(Long id) {
		return productoDao.findByIdFetch(id).orElse(null);
	}

	@Override
	@Transactional
	public ProductoEntity save(ProductoEntity entity) {
		return productoDao.save(entity);
	}

	@Override
	@Transactional
	public ProductoEntity update(ProductoEntity entity) {
		return productoDao.save(entity);
	}

	@Override
	@Transactional
	public void delete(ProductoEntity entity) {
		productoDao.delete(entity);
	}

	@Override
	public List<ProductoGenericDto> findTop10() {
		var resultados = productoDao.findTop10();

		List<ProductoGenericDto> productos = resultados.stream().map(r -> {
			ProductoGenericDto producto = new ProductoGenericDto();
			producto.setId(((BigInteger) r[0]).longValue());
			producto.setNombre((String) r[1]);
			producto.setTamano(Tamano.values()[(int) r[2]]);
			producto.setPrecioVenta((Double) r[3]);

			var cantidad = r[4];
			if (cantidad == null)
				producto.setCantidadVendida(0);
			else
				producto.setCantidadVendida(((BigDecimal) r[4]).longValue());
			return producto;
		}).collect(Collectors.toList());

		return productos;
	}

	@Override
	public List<ProductoGenericDto> findByNombreOrderByMasComprado(String nombre) {
		var resultados = productoDao.buscarPorNombreOrdenadoPorMasComprado(nombre);
		
		List<ProductoGenericDto> productos = resultados.stream().map(r -> {
			ProductoGenericDto producto = new ProductoGenericDto();
			producto.setId(((BigInteger) r[0]).longValue());
			producto.setNombre((String) r[1]);
			producto.setTamano(Tamano.values()[(int) r[2]]);
			producto.setPrecioVenta((Double) r[3]);

			return producto;
		}).collect(Collectors.toList());
		
		return productos;
	}

	@Override
	@Transactional
	public void bulkUpdate(List<ProductoEntity> productos) {
		for(int i=0;i<productos.size();i++) {		
			if(i> 0 && i % BATCH_SIZE == 0 || i == (productos.size()-1)) {
				entityManager.flush();
				entityManager.clear();
			}
			entityManager.merge(productos.get(i));
		}
	}

	@Override
	public void inactivarProducto(long id) {
		var result = productoDao.findById(id);
		
		if(result.isEmpty()) return;
		
		ProductoEntity producto = result.get();
		producto.setActivo(false);
		productoDao.save(producto);
	}

}
