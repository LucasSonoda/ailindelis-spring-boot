package com.lucassonoda.AilinDelis.producto;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IProductoDao extends JpaRepository<ProductoEntity, Long> {
	
	@Query("select p from ProductoEntity p order by p.activo desc, p.nombre")
	public List<ProductoEntity> findAllFetch();
	
	@Query("select p from ProductoEntity p where p.nombre like %:nombre% and p.activo = 1 order by p.nombre")
	public List<ProductoEntity> findByNombreContainingIgnoreCase(@Param("nombre") String nombre);

	@Query(value="select p.id, p.nombre, p.tamano, p.precio_venta "
			+ " from productos p left join pedido_items i on i.producto_id = p.id"
			+ " where p.nombre like %:nombre% and p.activo = 1"
			+ " group by p.id"
			+ " order by sum(i.cantidad) desc"
			+ "	limit 0,6", nativeQuery = true)
	public List<Object[]> buscarPorNombreOrdenadoPorMasComprado(@Param("nombre") String nombre);
	
	@Query("select p from ProductoEntity p left join fetch p.ingredientes where p.id = ?1")
	public Optional<ProductoEntity> findByIdFetch(long id);
	
	@Query(value = "SELECT p.id, p.nombre, p.tamano, p.precio_venta, sum(i.cantidad)"
			+ " FROM productos p left join pedido_items i on i.producto_id = p.id"
			+ " where p.activo = 1"
			+ " group by p.nombre"
			+ " order by sum(i.cantidad) desc"
			+ " limit 0,10 "
			, nativeQuery = true)
	public List<Object[]> findTop10();
		
}
