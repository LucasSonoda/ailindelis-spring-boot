package com.lucassonoda.AilinDelis.producto;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;
import com.lucassonoda.AilinDelis.utils.jsonview.View;

@Entity
@Table(name = "productos")
public class ProductoEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@JsonView({ View.PedidoListView.class, View.ProductoListView.class })
	private long id;
	@Column
	@JsonView({ View.PedidoSingleView.class, View.ProductoListView.class })
	private String nombre;
	@Column
	@JsonView({ View.PedidoSingleView.class, View.ProductoListView.class })
	private Tamano tamano;
	@Column
	@JsonView({ View.ProductoListView.class })
	private Double costo;
	@Column
	@JsonView({ View.PedidoListView.class, View.ProductoListView.class })
	private Double precioVenta;
	@Column
	@JsonView(View.PedidoListView.class)
	private int descuento;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "producto_id")
	private List<IngredienteEntity> ingredientes;

	public enum Tamano {
		Unico, Chico, Mediano, Grande
	}

	@Column(columnDefinition="tinyint(1) default 1")
	@JsonView({ View.PedidoListView.class, View.ProductoListView.class })
	private boolean activo;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "categoria_id")
	private CategoriaEntity categoria;

	public ProductoEntity() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Tamano getTamano() {
		return tamano;
	}

	public void setTamano(Tamano tamano) {
		this.tamano = tamano;
	}

	public Double getCosto() {
		return costo;
	}

	public void setCosto(Double costo) {
		this.costo = costo;
	}

	public Double getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public int getDescuento() {
		return descuento;
	}

	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}

	public List<IngredienteEntity> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(List<IngredienteEntity> ingredientes) {
		this.ingredientes = ingredientes;
	}

	public CategoriaEntity getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaEntity categoria) {
		this.categoria = categoria;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
