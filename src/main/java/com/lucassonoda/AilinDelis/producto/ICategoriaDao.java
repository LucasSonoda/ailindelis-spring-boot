package com.lucassonoda.AilinDelis.producto;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ICategoriaDao extends JpaRepository<CategoriaEntity, Long> {

	public List<CategoriaEntity> findByNombre(String nombre);
	
}
