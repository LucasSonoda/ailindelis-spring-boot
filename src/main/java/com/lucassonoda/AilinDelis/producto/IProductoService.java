package com.lucassonoda.AilinDelis.producto;

import java.util.List;

import com.lucassonoda.AilinDelis.interfaces.ICrudService;
import com.lucassonoda.AilinDelis.producto.dto.ProductoGenericDto;

public interface IProductoService extends ICrudService<ProductoEntity, Long> {
	public void inactivarProducto(long id);
	public List<ProductoGenericDto> findTop10();
	public List<ProductoGenericDto> findByNombreOrderByMasComprado(String nombre);
	
	public void bulkUpdate(List<ProductoEntity> productos);
}
