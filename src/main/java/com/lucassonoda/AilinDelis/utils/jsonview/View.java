package com.lucassonoda.AilinDelis.utils.jsonview;

public class View {
	public interface PedidoListView{}
	public interface PedidoByClienteView{}
	public interface PedidoSingleView extends PedidoListView{}
	public interface ClienteListView{}
	public interface ClienteSingleView extends ClienteListView{}
	public interface ProductoListView{}
}
