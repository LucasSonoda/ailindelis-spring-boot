package com.lucassonoda.AilinDelis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AilinDelisApplication {

	public static void main(String[] args) {
		SpringApplication.run(AilinDelisApplication.class, args);
	}

}
